//######################################################################################
//################################DS DEV # GULPTASKS####################################
//######################################################################################
var gulp       = require('gulp'),
    babel      = require("gulp-babel"),
    concat     = require("gulp-concat"),
    watch      = require('gulp-watch'),
    uglify     = require("gulp-uglify"),
    sass       = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps');
//############################################################################Setup dir path
var distBase   = '../',
    distScript  = 'script/',
    distBuild  = 'data/';
//############################################################################run Gulp
gulp.task('default',['sass','mobile','responsive','creator', 'js','jsComponents','watcher']);
//############################################################################Sass
//DS application css
gulp.task('sass',function(){
    return gulp.src('sass/application.sass')
    .pipe(sourcemaps.init())
    .pipe(sass({indentedSyntax: true, outputStyle: 'compact'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(distBase + distBuild));
});
//DS mobile landingpage CSS
gulp.task('mobile',function(){
    return gulp.src('sass/mobile.sass')
        //.pipe(sourcemaps.init())
        .pipe(sass({indentedSyntax: true, outputStyle: 'compressed'}))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(distBase + distBuild));
});
//DS Responsive CSS
gulp.task('responsive',function(){
    return gulp.src('sass/responsive.sass')
        //.pipe(sourcemaps.init())
        .pipe(sass({indentedSyntax: true, outputStyle: 'compressed'}))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(distBase + distBuild));
});
//DS Creator CSS
gulp.task('creator',function(){
    return gulp.src('sass/creator.sass')
        //.pipe(sourcemaps.init())
        .pipe(sass({indentedSyntax: true, outputStyle: 'compressed'}))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(distBase + distBuild));
});
//############################################################################js
gulp.task('js',function(){
    return gulp.src(['js/_polyfill.js','js/_loader.js','js/_version.js'])
    //.pipe(sourcemaps.init())
    .pipe( concat('application.js'))
    .pipe( babel() )
    .pipe( uglify() )
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest( distBase + distScript ));
});
//############################################################################jsComponents
gulp.task('jsComponents',function(){
    return gulp.src('js/components/*.js')
    //.pipe(sourcemaps.init())
    .pipe( concat('components.js'))
    .pipe( babel() )
    .pipe( uglify() )
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest( distBase + distScript ));
});
//############################################################################watcher
gulp.task('watcher',function(){
    gulp.watch('sass/**/*.sass',['sass', 'mobile', 'responsive', 'creator']);
    gulp.watch('js/*.js',['js']);
    gulp.watch('js/**/*.js',['jsComponents']);
});